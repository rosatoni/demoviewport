package cat.escolapia.pmdm;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.badlogic.gdx.utils.viewport.FillViewport;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

public class DemoViewport extends ApplicationAdapter implements InputProcessor {
	SpriteBatch batch;
	Sprite aspectRatios;
	OrthographicCamera camera;
	Viewport viewport;
    String c_viewport;
    private Texture texture;
    private BitmapFont font;

	@Override
	public void create () {
		batch = new SpriteBatch();
        Pixmap pixmap = new Pixmap(50, 50, Pixmap.Format.RGBA8888);
        pixmap.setColor(Color.BLUE);
        pixmap.fillRectangle(0, 0, 50, 50);
        texture = new Texture(pixmap);
        pixmap.dispose();

        font = new BitmapFont();
        font.setColor(Color.RED);
        font.getData().setScale(0.25f);

		aspectRatios = new Sprite(new Texture(Gdx.files.internal("aspect.jpg")));
		aspectRatios.setPosition(0,0);
		aspectRatios.setSize(192, 108);

		camera = new OrthographicCamera();
        c_viewport = "FillViewport";
		viewport = new FillViewport(192,108,camera);
		viewport.apply();
        camera.position.set(camera.viewportWidth/2,camera.viewportHeight/2,0);

        Gdx.input.setInputProcessor(this);
	}

	@Override
	public void render () {

        if (Gdx.input.isKeyJustPressed(Input.Keys.NUM_1)) {
            c_viewport = "FillViewport";
            viewport = new FillViewport(192,108, camera);
            viewport.apply();
            viewport.update(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
            camera.position.set(camera.viewportWidth / 2, camera.viewportHeight / 2, 0);
        } else  if (Gdx.input.isKeyJustPressed(Input.Keys.NUM_2)) {
            c_viewport = "ExtendViewport";
            viewport = new ExtendViewport(192,108,camera);
            viewport.apply();
            viewport.update(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
            camera.position.set(camera.viewportWidth / 2, camera.viewportHeight / 2, 0);
        } else  if (Gdx.input.isKeyJustPressed(Input.Keys.NUM_3)) {
            c_viewport = "FitViewport";
            viewport = new FitViewport(192,108, camera);
            viewport.apply();
            viewport.update(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
            camera.position.set(camera.viewportWidth / 2, camera.viewportHeight / 2, 0);
        } else  if (Gdx.input.isKeyJustPressed(Input.Keys.NUM_4)) {
            c_viewport = "StretchViewport";
            viewport = new StretchViewport(192,108,camera);
            viewport.apply();
            viewport.update(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
            camera.position.set(camera.viewportWidth / 2, camera.viewportHeight / 2, 0);
        } else  if (Gdx.input.isKeyJustPressed(Input.Keys.NUM_5)) {
            c_viewport = "ScreenViewport";
            viewport = new ScreenViewport(camera);
            viewport.apply();
            viewport.update(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
            camera.position.set(camera.viewportWidth / 2, camera.viewportHeight / 2, 0);
        }

		camera.update();
        Gdx.gl.glClearColor(1, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        batch.setProjectionMatrix(camera.combined);
		batch.begin();

        aspectRatios.draw(batch);
        batch.draw(texture, 0, 0, texture.getWidth(),texture.getHeight());
        font.draw(batch, "1-Fillview | 2-Extendedview | 3-Fitview | 4-Strechview | 5-Screenview", 10, 10);
		batch.end();
	}

	@Override
	public void dispose(){
		aspectRatios.getTexture().dispose();
	}

	@Override
	public void resize(int width, int height){
		viewport.update(width,height);
		camera.position.set(camera.viewportWidth/2,camera.viewportHeight/2,0);
	}

    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        Gdx.app.log("Mouse Event","Click a la finestra " + screenX + "," + screenY);
        Vector3 worldCoordinates = camera.unproject(new Vector3(screenX,screenY,0));
        Gdx.app.log("Mouse Event","Projectat a " + worldCoordinates.x + "," + worldCoordinates.y);
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }
}
